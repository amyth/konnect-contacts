#!/usr/bin/env python

import datetime
import json
import syslog

import redis
import tinys3


class Mover(object):
    """
    Moves the contact objects from redis to Amazon s3
    """

    def __init__(self, s3bucket, key, secret,
            redis_host='172.22.67.80',
            redis_port=6379,
            redis_db=1):
        """
        Initializes the required connections
        """

        self.keys = []
        self.date_format = '%d/%m/%y %H:%M:%S'
        self.bucket = bucket_name
        self.s3_conn = tinys3.Connection(key, secret, default=self.bucket)
        self.redis_conn = redis.StricRedis(host=redis_host, port=redis_port,
                db=redis_db)

    def _log_error(self, message):
        """
        Logs the error into syslog
        """
        syslog.syslog(syslog.LOG_ERR, message)

    def get_keys_to_move(self):
        """
        Updates self.keys list with all the keys that needs
        to be moved to s3
        """

        self.keys = list(self.redis_conn.smembers('keys_to_port', 0, -1))

    def move(self):
        """
        Moves all redundant contacts data to s3
        """

        self.get_keys_to_move()
        for key in self.keys:
            self.move_data(key)

    def move_data(self, key):
        """
        Moves a single contact data object as a json file
        to s3
        """

        data = redis_conn.hget(key, 'data')

        try:
            json_data = json.loads(data)
            self.s3_conn.upload('%s.json' % key, data)
            now = datetime.datetime.now()
            uploaded_at = now.strftime(self.date_format)
            redis_conn.hset(key, 's3upload', uploaded_at)
            redis_conn.hdel(key, 'data')
            redis_conn.srem('keys_to_port', key)
        except ValueError as err:
            self._log_error("JsonError: key='%s', err='%s'" % (key, str(err)))
